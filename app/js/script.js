const state = {
    products: {
        water: {
            price: 0.65,
            amount: 20
        },
        sprite: {
            price: 1.00,
            amount: 20
        },
        pepsi: {
            price: 1.50,
            amount: 20
        }
    },
    money: {
        nickel: {
            value: 0.05,
            amount: 50
        },
        dime: {
            value: 0.10,
            amount: 50
        },
        quarter: {
            value: 0.25,
            amount: 50
        },
        dollar: {
            value: 1.00,
            amount: 50
        }
    }
};

const totalAmount = {
    sum: 0,
    nickel: 0,
    dime: 0,
    quarter: 0,
    dollar: 0
};

const coinInput = document.getElementById("coin-input");
const coinAlert = document.getElementById("coin-alert");
const coinRest = document.getElementById("coin-rest");
const coinSum = document.getElementById("coin-sum");

const changeInputValue = (e, x) => {
    coinInput.value = x;
    let y = document.getElementsByClassName("coin");
    manageActiveElement(e, y);
};

const manageActiveElement = (e, y) => {
    let i = _.findIndex(y, function (x) {
        return x.classList.contains("active");
    });
    if (i !== -1) {
        y[i].classList.remove("active");
    }
    e.classList.add("active");
};

const handleInsert = () => {
    if (validate(Number(coinInput.value))) {
        updateTotalAmount(Number(coinInput.value));
        coinSum.textContent = totalAmount.sum.toFixed(2);
    } else {
        coinAlert.textContent = "Insert correct coin";
    }
};

const updateTotalAmount = e => {
    totalAmount.sum += e;
    switch(e) {
        case 0.05:
            totalAmount.nickel ++;
            break;
        case 0.1:
            totalAmount.dime ++;
            break;
        case 0.25:
            totalAmount.quarter ++;
            break;
        case 1:
            totalAmount.dollar ++;
            break;
    }
};

const validate = e => {
    let correctCoin = [0.05, 0.1, 0.25, 1];
    if (correctCoin.includes(e)) {
        return true;
    } else {
        coinAlert.textContent = "Insert correct coin";
        return false;
    }
};

const buyProduct = x => {
    let p = state.products[x];
    if (p.price <= totalAmount.sum && p.amount >= 1) {
        addAndRemoveClass(x);
        spendTheRest(p);
        updateStateMoney();
        p.amount --;
        resetTotalAmount();
        coinSum.textContent = totalAmount.sum;
    } else {
        if (p.price > totalAmount.sum) {
            coinAlert.textContent = "You need to insert more coins";
            allertClean(coinAlert, 3000);
        } else if (p.amount < 1) {
            coinAlert.textContent = "Product unavailable, please choose different";
            allertClean(coinAlert, 3000);
        }

    }
};

const allertClean = (a, b) => {
    setTimeout(function () {
        a.innerText = "";
    }, b)
}

const addAndRemoveClass = x => {
    let y = document.getElementById(x);
    y.classList.add("bought");
    setTimeout(function () {
        y.classList.remove("bought");
    }, 2000)
};

const spendTheRest = p => {
    let a, x, dollarRest, quarterRest, dimeRest, nickelRest;
    x = totalAmount.sum - p.price;
    a = x.toFixed(2);
    dollarRest = Math.floor(a / 1.00);
    totalAmount.dollar -= dollarRest;
    a = (a % 1).toFixed(2);
    quarterRest = Math.floor(a / 0.25);
    totalAmount.quarter -= quarterRest;
    a = (a % 0.25).toFixed(2);
    dimeRest = Math.floor(a / 0.1);
    totalAmount.dime -= dimeRest;
    a = (a % 0.1).toFixed(2);
    nickelRest = Math.floor(a / 0.05);
    totalAmount.nickel -= nickelRest;
    a = (a % 0.05).toFixed(2);
    coinRest.innerText = x.toFixed(2) + "$" + "\nNickel: " + nickelRest + "\nDime: " + dimeRest + "\nQuarter: " + quarterRest + "\nDollar: " + dollarRest;
    allertClean(coinRest, 5000);
};

const updateStateMoney = () => {
    state.money.nickel.amount += totalAmount.nickel;
    state.money.dime.amount += totalAmount.dime;
    state.money.quarter.amount += totalAmount.quarter;
    state.money.dollar.amount += totalAmount.dollar;
};

const resetTotalAmount = () => {
    for (let i in totalAmount) {
        if (totalAmount.hasOwnProperty(i)) {
            totalAmount[i] = 0;
        }
    }
};

const coinReturn = () => {
    coinRest.innerText = totalAmount.sum.toFixed(2) + "$" + "\nNickel: " + totalAmount.nickel + "\nDime: " + totalAmount.dime + "\nQuarter: " + totalAmount.quarter + "\nDollar: " + totalAmount.dollar;
    resetTotalAmount();
    document.getElementById("coin-sum").textContent = "0";
    allertClean(coinRest, 4000);
};

const showService = () => {
    setInputValueService ("pepsi");
    setInputValueService ("sprite");
    setInputValueService ("water");
    setInputValueService ("nickel");
    setInputValueService ("dime");
    setInputValueService ("quarter");
    setInputValueService ("dollar");
    document.getElementById("show-service").style.display = "none";
    document.getElementById("service").style.display = "flex";
};

const setInputValueService = a => {
    if (Object.keys(state.products).includes(a)) {
        document.getElementById(a + "-service").setAttribute("value", state.products[a].amount);
    } else {
        document.getElementById(a + "-service").setAttribute("value", state.money[a].amount);
    }
};

const updateStateServiceClick = () => {
    updateStateService ("pepsi");
    updateStateService ("sprite");
    updateStateService ("water");
    updateStateService ("nickel");
    updateStateService ("dime");
    updateStateService ("quarter");
    updateStateService ("dollar");
    document.getElementById("service").style.display = "none";
    document.getElementById("show-service").style.display = "inline-block";
};

const updateStateService = a => {
    if (Object.keys(state.products).includes(a)) {
        state.products[a].amount = Number(document.getElementById(a + "-service").value);
    } else {
        state.money[a].amount = Number(document.getElementById(a + "-service").value);
    }
};
